--------------------------------------------------
-- readNumbers.hs
-- Casey Miles
-- Dec. 6, 2012
--
-- This program reads lists of x and y values from
-- a user-defined text file, and outputs a 500x500
-- graph of the data and its best-fit line on an 
-- HTML5 canvas element at localhost:3000.
--------------------------------------------------

import System.IO
import Statistics.LinearRegression
import System.Environment
import qualified Data.Vector.Unboxed as U
import Graphics.Blank
import Text.Printf
import GHC.Float


main :: IO ()
main = do
  args <- getArgs
  let filename = args !! 0

  content <- readFile filename
  let contentLines = lines content

  -- Create coordinate lists by reading the words of each line.
  -- Then create coordinate tuples by zipping those lists.
  let xs = readFloat (words (contentLines !! 0))
  let ys = readFloat (words (contentLines !! 1))
  let coords = zip xs ys

  -- Calculate the range of both xs and ys
  let xRange = (maximum xs) - (minimum xs)
  let yRange = (maximum ys) - (minimum ys)

  -- Shift the x and y coordinate groups to avoid issues with negative numbers.
  -- Then scale the shifted coordinates to a scale [0, 500] for display in canvas.
  -- Zip the shifted, scaled lists for displayable coordinate tuples.
  let xsShift = map (\x -> x + (abs (minimum xs))) xs
  let ysShift = map (\y -> y + (abs (minimum ys))) ys

  let xsScaled = map (\x -> scaleValues x (maximum xsShift) (minimum xsShift)) xsShift
  let ysScaled = map (\y -> 500 - scaleValues y (maximum ysShift) (minimum ysShift)) ysShift
  let plottableCoords = zip xsScaled ysScaled

  -- Notify user that data will be plotted on localhost:3000
  putStrLn "Open localhost:3000 to see plot and best fit. CTRL-C to quit."

  -- Plot the data.
  blankCanvas 8000 $ \ context -> do
    send context $ do
	
	-- Draw the grid, a 500x500 square with gridlines every 50 pixels.
	beginPath()
	drawGrid [0,50..500]

	-- Write labels for the x and y gridlines.
	beginPath()
	fillStyle "#444444"
	writeXLabels xRange xs
	writeYLabels yRange ys
		
	-- Draw the points using the scaled coordinates created earlier.	
	beginPath()
	sequence_ (map drawPoint plottableCoords)

	-- Draw the line of best fit. Use scaled values for correct graphing.
	beginPath()
	drawBestFit xsScaled ysScaled

	-- Print the equation for best fit. Use unscaled values to provide correct equation.
	beginPath()
	writeBestFit xs ys

  return()



-- Converts list of Strings to list of Float numbers.
readFloat :: [String] -> [Float]
readFloat = map read

-- Scales a value to its proportional equivalent in a range of [0,500].
scaleValues :: Float -> Float -> Float -> Int
scaleValues x max min = round (500 * ((x - min) / (max - min)))

-- Divides the range of a list of numbers by 10, then writes grid markers every 50 pixels.
writeXLabels :: Float -> [Float] -> Canvas ()
writeXLabels range xs = do
   let xIncr = range / 10
   let xmin = minimum xs
   -- There are 11 gridlines to label. Take 11 of a list of tenth-steps from min to max.
   -- Zip those with a list of where the x gridlines are, and perform each fillText command in the resulting list.
   let labelCommand = zipWith (\ val x -> fillText((printf "%.2f" val), x, 520)) (take 11 [xmin, (xmin + xIncr)..]) [0, 50..500]
   sequence_ labelCommand

-- Divides the range of a list of numbers by 10, then writes grid markers every 50 pixels.
writeYLabels :: Float -> [Float] -> Canvas ()
writeYLabels range ys = do
   let yIncr = range / 10
   let ymin = minimum ys
   -- There are 11 gridlines to label. Take 11 of a list of tenth-steps from min to max.
   -- Zip those with a list of where the y gridlines are (offset by 7 so they are still visible).
   -- Then perform each fillText command in the resulting list.
   let labelCommand = zipWith (\ val y -> fillText((printf "%.2f" val), 510, y)) (take 11 [ymin, (ymin + yIncr)..]) [507, 457..7]
   sequence_ labelCommand

-- Draws a grid on the canvas, given a list of gridline locations.
drawGrid :: [Float] -> Canvas ()
drawGrid xs = do
   let max = maximum xs
   let min = minimum xs

   --Draw the box.
   moveTo(min, min)
   lineTo(min, max)
   lineTo(max, max)
   lineTo(max, min)
   lineTo(min, min)
   lineWidth 3
   strokeStyle "#bbbbbb"
   stroke()

   --Draw gridlines
   let moveCommands = map (\x -> moveTo(x, 0)) xs
   let lineCommands = map (\x -> lineTo(x, 500)) xs
   sequence_ (alternate moveCommands lineCommands)
   
   let moveCommands = map (\y -> moveTo(0, y)) xs
   let lineCommands = map (\y -> lineTo(500, y)) xs
   sequence_ (alternate moveCommands lineCommands)
   
   lineWidth 1
   strokeStyle "#cdcdcd"
   stroke()


-- Takes two lists and creates a new list of their alternated elements.
alternate :: [a] -> [a] -> [a]
alternate (x:xs) ys = x:(alternate ys xs)
alternate _ _ = []
   
-- Draws a point on the canvas. Takes Int tuple for cleaner display on canvas.
drawPoint :: (Int, Int) -> Canvas ()
drawPoint (x, y) = do
   moveTo(fromIntegral x, fromIntegral y)
   arc(fromIntegral x, fromIntegral y, 1, 0, 2 * pi, False)
   lineWidth 3
   strokeStyle "steelBlue"
   stroke()

-- Draw the best-fit line on the canvas. Takes a list of the SCALED x and y values.
drawBestFit :: [Int] -> [Int] -> Canvas ()
drawBestFit xs ys = do
   -- linearRegression only takes vectors of Doubles.
   let xsDoub = map (\x -> fromIntegral x) xs
   let ysDoub = map (\y -> fromIntegral y) ys
   let xsU = U.fromList xsDoub
   let ysU = U.fromList ysDoub
   let regResult = linearRegressionRSqr xsU ysU

   -- Generate a list of displayable x-values (values whose corresponding y-values are within the grid area).
   -- Then draw the line from the smallest x-value to the largest.
   let lineList = [x | x <- [0..500], 0 <= calcSlopeInt regResult x, calcSlopeInt regResult x <= 500]
   moveTo((minimum lineList), (calcSlopeInt regResult (minimum lineList)))
   lineTo((maximum lineList), (calcSlopeInt regResult (maximum lineList)))

   lineWidth 3
   strokeStyle "gold"
   stroke()

-- Applies a simple y=a+bx equation.
calcSlopeInt :: (Double, Double, Double) -> Float -> Float
calcSlopeInt (alpha, beta, _) x = (double2Float alpha) + ((double2Float beta) * x)

-- Writes the best-fit equation on the canvas. Takes a list of the ORIGINAL x and y values.
writeBestFit :: [Float] -> [Float] -> Canvas ()
writeBestFit xs ys = do
  -- linearRegression only takes vectors of Doubles.
  let xsDoub = map (\x -> float2Double x) xs
  let ysDoub = map (\y -> float2Double y) ys
  let xsU = U.fromList xsDoub
  let ysU = U.fromList ysDoub
  let regResult =  linearRegressionRSqr xsU ysU
  let regString = (\ (alpha, beta, rSqr) -> (printf "Best fit: y = %.4f" beta) ++ (printf "x + %.4f" alpha) ++ (printf ", r^2 = %.4f" rSqr)) regResult

  font "12pt sans-serif"
  textAlign "center"
  fillStyle "#444444"
  fillText(regString, 250, 560)

